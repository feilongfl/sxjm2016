function a = Q3Run2(t)%������
    a(t,4) = 0;
    parfor i = 1:t
        %{
        for j = 1:t
            [~,aa,~] = Q3(1200 + 100 * i,210 + 30 * j);
            c = aa(5) * 180 / pi;
            d = aa(length(aa)) * 180 / pi;
            a(i,:) = [c,d,1200 + 100 * i,210 + 30 * j];
        end
        %}
        subQ3(i,t)
    end
end

function b = subQ3(ii,t)
    parfor i = 1:t
        [~,aa,~] = Q3(1200 + 100 * ii,210 + 30 * i);
        c = aa(5) * 180 / pi;
        d = aa(length(aa)) * 180 / pi;
        b(i,:) = [c,d,1200 + 100 * ii,210 + 30 * i];
    end
end
