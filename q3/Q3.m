function [hh,a,p] = Q3(vQ3BallWeight,vQ3AnchorChainLength)
    %%%清空运行环境
    %clc
    %clear
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%常     量%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 浮标
    cBuoyDiameter       = 2;    %浮标底面直径(m)
    cBuoyHeight         = 2;    %浮标高度(m)
    cBuoyVolume         = (cBuoyDiameter / 2) ^ 2 * cBuoyHeight * pi; 
                                %浮标体积(m^3)
    cBuoyWeight         = 1000; %浮标质量(kg)
    %钢管
    cPipeLength         = 1;    %钢管长度(m)
    cPipeDiameter       = 0.05; %钢管直径(m)
    cPipeVolume         = (cPipeDiameter / 2) ^ 2 * cPipeLength * pi;
                                %钢管体积(m^3)
    cPipeWeight         = 10;   %钢管质量(kg)
    %锚
    cAnchorWeight       = 600;  %锚质量(kg)
    %锚链
    cAnchorChain1Length = 0.078;%锚链1长度(m)
    cAnchorChain2Length = 0.105;%锚链2长度(m)
    cAnchorChain3Length = 0.120;%锚链3长度(m)
    cAnchorChain4Length = 0.150;%锚链4长度(m)
    cAnchorChain5Length = 0.180;%锚链5长度(m)
    cAnchorChain1Weight = 3.2;  %锚链1质量(kg/m)
    cAnchorChain2Weight = 7;    %锚链2质量(kg/m)
    cAnchorChain3Weight = 12.5; %锚链3质量(kg/m)
    cAnchorChain4Weight = 19.5; %锚链4质量(kg/m)
    cAnchorChain5Weight = 28.12;%锚链5质量(kg/m)
    %钢桶
    cBarrelLength       = 1;    %钢桶长度(m)
    cBarrelDiameter     = 0.3;  %钢桶外径(m)
    cBarrelVolume       = (cBarrelDiameter / 2) ^ 2 * cBarrelLength * pi;
                                %钢桶体积(m^3)
    cBarrelWeight       = 100;  %钢桶质量(kg)
    %其他
    cSeaDensity         = 1.025 * 10 ^ 3;
                                %海水密度(kg / m^3)
    cStellDensity         = 8 * 10 ^ 3;
                                %钢密度(kg / m^3)
    g=9.7803267714;%地球重力常数

    %%%%%%%%%%%%%%%%%%%%%%%%Q1%%%%%%%%%%%%%%%%%%%%%%%%
    %cQ1AnchorChainLength  = 22.05; %锚链长度(m)
    cQ1AnchorChainLength  = vQ3AnchorChainLength;
    cQ1AnchorChainQuantity= int16(cQ1AnchorChainLength / cAnchorChain2Length);
                                   %锚链数量(个)
    %cQ1BallWeight         = 1200;  %重物球质量(kg)
    cQ1BallWeight          = vQ3BallWeight;
    %symb cQ1BallWeight
    cQ1Depth              = 18;    %水深(m)
    cQ1SeaWind1           = 12;    %风速1(m/s)
    cQ1SeaWind2           = 24;    %风速2(m/s)
    cQ1SeaWind3           = 36;    %风速3(m/s)
    cQ1SeaWind            = cQ1SeaWind3;%风速选择
    cQ1SeaWater           = 0;     %水速(m/s)
    %重力矩阵
    cQ1Chain = cQ1AnchorChainQuantity + 5;%钢管*4+桶+链子
    G = zeros(int16(cQ1Chain),1);
    G(1:4) = cPipeWeight * g - cSeaDensity * g * cPipeVolume;
    G(5) = cBarrelWeight * g - cSeaDensity * g * cBarrelVolume + cQ1BallWeight * g * (1 - cSeaDensity / cStellDensity);
    G(6:int16(cQ1Chain)) = cAnchorChain2Weight * cAnchorChain2Length * g * (1 - cSeaDensity / cStellDensity);
    %长度矩阵
    L = zeros(int16(cQ1Chain),1);
    L(1:4) = cPipeLength;
    L(5) = cBarrelLength;
    L(6:int16(cQ1Chain)) = cAnchorChain2Length;

    %x = solve('-(cQ1Chain -3)*(cBuoyWeight * g - cSeaDensity * g * pi * x)-(4 * cQ1Chain - 14) * G(1)-(cQ1Chain -6)*G(5)-(cQ1Chain-6)*(cQ1Chain-7)/2*G(6) = (cQ1Depth - 5) / L(6)*(0.625*2*(2-x)*12^2)');
    %x = solve('-(cQ1Chain -3)*(cBuoyWeight * g - cSeaDensity * g * pi * x)-(4 * cQ1Chain - 14) * (cPipeWeight * g - cSeaDensity * g * cPipeVolume)-(cQ1Chain -6)*(cBarrelWeight * g - cSeaDensity * g * cBarrelVolume + cQ1BallWeight * g * (1 - cSeaDensity / cStellDensity))-(cQ1Chain-6)*(cQ1Chain-7)/2*(cAnchorChain2Weight * g * (1 - cSeaDensity / cStellDensity)) = (cQ1Depth - 5 - x) / cAnchorChain2Length*(0.625*2*(2-x)*12^2)','x')

    %%G(0) = cBuoyWeight * g - cSeaDensity * g * pi * x;
    %b = cBuoyWeight * g - cSeaDensity * g * pi * x;
    %W = 0.625*2*(2-x)*12^2;

    %a = zeros(int16(cQ1Chain),1);
    %a(1) = solve('W * tan(a(1)) + b = 0');
    %G0 = sym('G0')


    syms h
    G0 = cBuoyWeight * g - cSeaDensity *g *pi*h;
    W = 0.625 * (4 - 2 * h) * cQ1SeaWind^2 + 374 * (2 * h + cBarrelDiameter * cBarrelLength) * cQ1SeaWater^2;

    %H = zeros(int16(cQ1Chain),1);
    H(int16(cQ1Chain)) = G0;%声明
    parfor i = 1:int16(cQ1Chain)
    %for i = 1:2%%测试用
        GG = G0;
        if i ~= 1
            %for j = 1:i
            for j = 1:i - 1%%%%%%%%%%%%warrning
                GG = GG + G(j);
            end
        end
        H(i) = L(i) * (GG) / sqrt(W^2 + GG^2);
        %i%test
    end

    HH = 0;%声明
    for i = 1:int16(cQ1Chain)
    %for i = 1:2%%测试用
        HH = HH + H(i);
        %i%test
    end
    HH = HH + cQ1Depth - h;
    HHstr = [char(HH) , ' = 0']             ;
    hh = solve(HHstr,'h')                   ;
    %hh = 0.68744;%test

    a(int16(cQ1Chain)) = 0;%与水平夹角
    parfor i = 1:int16(cQ1Chain)%钢管*4
        temp = G0;
        for j = 1:i-1
            temp = temp + G(j);
        end
        
        if atan(-subs(temp,hh) / subs(W,hh)) >= 0%链子未沉在海底
            a(i) = atan(-subs(temp,hh) / subs(W,hh));
        end
    end
    a = pi / 2 - a;%换算到与竖直线夹角
    %plot(1:int16(cQ1Chain),a);

    p(int16(cQ1Chain),2) = 0;
    parfor i = 1:int16(cQ1Chain)
        px = 0;
        py = 0;
        for j = i:int16(cQ1Chain)
            px = px + L(j)*sin(a(j));
            py = py + L(j)*cos(a(j));
        end
        p(i,:) = [px,py];
    end
    x = p(6:int16(cQ1Chain),1);%舍去管子、浮标
    y = p(6:int16(cQ1Chain),2);
    %plot(x,y);
    %{
    z = find(p(:,2) ~= 0);
    x = p(6:int16(cQ1Chain) - z,1);%舍去管子、浮标
    y = p(6:int16(cQ1Chain) - z,2);
    [p,S] = polyfit(y,x,20);%拟合;
    plot(x,y);
    plot(polyval(p,y),y);
    S.normr
    %}
    
    %%浮动区域
    %p(1)
    
end