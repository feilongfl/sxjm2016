function a = Q3Run(t,p,o,l,k)%采样数,重物初始、步进，链长初始、步进
    a(t,4) = 0;
    parfor i = 1:t
        for j = 1:t
            [~,aa,~] = Q3(p + o * i,l + k * j);
            c = aa(5) * 180 / pi;
            d = aa(length(aa)) * 180 / pi;
            a(i,:) = [c,d,p + o * i,l + k * j];
        end
    end
    load chirp;
    sound(y,Fs);
end