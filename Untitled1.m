%%%清空运行环境
clc
clear
%%%%%%%%%%%%%%%%%%%%%%%%%%%%常     量%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 浮标
cBuoyDiameter       = 2;    %浮标底面直径(m)
cBuoyHeight         = 2;    %浮标高度(m)
cBuoyVolume         = (cBuoyDiameter / 2) ^ 2 * cBuoyHeight * pi; 
                            %浮标体积(m^3)
cBuoyWeight         = 1000; %浮标质量(kg)
%钢管
cPipeLength         = 1;    %钢管长度(m)
cPipeDiameter       = 0.05; %钢管直径(m)
cPipeVolume         = (cPipeDiameter / 2) ^ 2 * cPipeLength * pi;
                            %钢管体积(m^3)
cPipeWeight         = 10;   %钢管质量(kg)
%锚
cAnchorWeight       = 600;  %锚质量(kg)
%锚链
cAnchorChain1Length = 0.078;%锚链1长度(m)
cAnchorChain2Length = 0.105;%锚链2长度(m)
cAnchorChain3Length = 0.120;%锚链3长度(m)
cAnchorChain4Length = 0.150;%锚链4长度(m)
cAnchorChain5Length = 0.180;%锚链5长度(m)
cAnchorChain1Weight = 3.2;  %锚链1质量(kg/m)
cAnchorChain2Weight = 7;    %锚链2质量(kg/m)
cAnchorChain3Weight = 12.5; %锚链3质量(kg/m)
cAnchorChain4Weight = 19.5; %锚链4质量(kg/m)
cAnchorChain5Weight = 28.12;%锚链5质量(kg/m)
%钢桶
cBarrelLength       = 1;    %钢桶长度(m)
cBarrelDiameter     = 0.3;  %钢桶外径(m)
cBarrelVolume       = (cBarrelDiameter / 2) ^ 2 * cBarrelLength * pi;
                            %钢桶体积(m^3)
cBarrelWeight       = 100;  %钢桶质量(kg)
%其他
cSeaDensity         = 1.025 * 10 ^ 3;
                            %海水密度(kg / m^3)
cStellDensity         = 8 * 10 ^ 3;
                            %钢密度(kg / m^3)
g=9.7803267714;%地球重力常数

%%%%%%%%%%%%%%%%%%%%%%%%Q1%%%%%%%%%%%%%%%%%%%%%%%%
cQ1AnchorChainLength  = 22.05; %锚链长度(m)
cQ1AnchorChainQuantity= cQ1AnchorChainLength / cAnchorChain2Length;
                               %锚链数量(个)
cQ1BallWeight         = 1200;  %重物球质量(kg)
cQ1Depth              = 18;    %水深(m)
cQ1SeaWind1           = 12;    %风速1(m/s)
cQ1SeaWind2           = 24;    %风速2(m/s)
cQ1SeaWater           = 0;     %水速(m/s)
%重力矩阵
cQ1Chain = cQ1AnchorChainQuantity + 5;%钢管*4+桶+链子
G = zeros(int16(cQ1Chain),1);
G(1:4) = cPipeWeight * g - cSeaDensity * g * cPipeVolume;
G(5) = cBarrelWeight * g - cSeaDensity * g * cBarrelVolume + cQ1BallWeight * g * (1 - cSeaDensity / cStellDensity);
G(6:int16(cQ1Chain)) = cAnchorChain2Weight * cAnchorChain2Length * g * (1 - cSeaDensity / cStellDensity);
%长度矩阵
L = zeros(int16(cQ1Chain),1);
L(1:4) = cPipeLength;
L(5) = cBarrelLength;
L(6:int16(cQ1Chain)) = cAnchorChain2Length;

%x = solve('-(cQ1Chain -3)*(cBuoyWeight * g - cSeaDensity * g * pi * x)-(4 * cQ1Chain - 14) * G(1)-(cQ1Chain -6)*G(5)-(cQ1Chain-6)*(cQ1Chain-7)/2*G(6) = (cQ1Depth - 5) / L(6)*(0.625*2*(2-x)*12^2)');
%x = solve('-(cQ1Chain -3)*(cBuoyWeight * g - cSeaDensity * g * pi * x)-(4 * cQ1Chain - 14) * (cPipeWeight * g - cSeaDensity * g * cPipeVolume)-(cQ1Chain -6)*(cBarrelWeight * g - cSeaDensity * g * cBarrelVolume + cQ1BallWeight * g * (1 - cSeaDensity / cStellDensity))-(cQ1Chain-6)*(cQ1Chain-7)/2*(cAnchorChain2Weight * g * (1 - cSeaDensity / cStellDensity)) = (cQ1Depth - 5 - x) / cAnchorChain2Length*(0.625*2*(2-x)*12^2)','x')

%%G(0) = cBuoyWeight * g - cSeaDensity * g * pi * x;
%b = cBuoyWeight * g - cSeaDensity * g * pi * x;
%W = 0.625*2*(2-x)*12^2;

%a = zeros(int16(cQ1Chain),1);
%a(1) = solve('W * tan(a(1)) + b = 0');
%G0 = sym('G0')


syms h
G0 = cBuoyWeight * g - cSeaDensity *g *pi*h
W = 0.625 * (4 - 2 * h) * cQ1SeaWind1^2 + 374 * 2 * h * cQ1SeaWater^2

%H = zeros(int16(cQ1Chain),1);
H(1) = G0;%声明
for i = 1:int16(cQ1Chain)
%for i = 1:2%%测试用
    GG = G0;
    if i ~= 1
        %for j = 1:i
        for j = 1:i - 1%%%%%%%%%%%%warrning
            GG = GG + G(j);
        end
    end
    H(i) = L(i) * (GG) / sqrt(W^2 + GG^2);
    %i%test
end

HH = 0;%声明
for i = 1:int16(cQ1Chain)
%for i = 1:2%%测试用
    HH = HH + H(i);
    %i%test
end
HH = HH + cQ1Depth - h;
HHstr = [char(HH) , ' = 0']
%hh = solve(HHstr,'h')
hh = 0.68744;%test

a(1) = 0;%声明
for i = 1:int16(cQ1Chain)%钢管*4
    temp = G0;
    for j = 1:i
        temp = temp + G(j);
    end
    a(i) = atan(-subs(temp,hh) / subs(W,hh));
end

p(1,1) = 0;
for i = 1:int16(cQ1Chain)
    j = int16(cQ1Chain) - i + 1;
    p(j,1) = 0;
    p(j,2) = 0;
    for k = j:int16(cQ1Chain)
        p(j,1) = p(j,1) + L(k)*cos(a(k));
        p(j,2) = p(j,2) + L(k)*sin(a(k));
    end
end










