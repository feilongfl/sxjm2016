function [b] = Q2ans ()
    b = Q2Run(30);
    b(:,3) = 90 - b(:,2);%角度换算
    b(:,4) = 1200:50:1200 + 50 * (length(b(:,1)) - 1);
    
    %最小二乘法拟合
    y1 = b(:,1);%桶角
    y2 = b(:,3);%锚角
    x = b(:,4);%重物质量
    
    figure;
    plot(x,y1);
    figure
    plot(x,y2);
    
    p1 = polyfit(y1,x,2);
    y11 = polyval(p1,5);
    p2 = polyfit(y2,x,2);
    y22 = polyval(p2,16);
end
