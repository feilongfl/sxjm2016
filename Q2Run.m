function a = Q2Run(t)%������
    a(t,2) = 0;
    parfor i = 1:t
        [~,aa,~] = Q2(1200 + 50 * i);
        c = aa(5) * 180 / pi;
        d = aa(length(aa)) * 180 / pi;
        a(i,:) = [c,d];
    end
end
